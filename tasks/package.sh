#!/bin/bash
set -e -u -x

SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
. ${SCRIPTS_DIR}/settings.sh

env

# Generate jars
#  maven-resource does not have option to upload javadoc and source
#mvn -DskipTests=true package javadoc:jar source:jar
mvn -DskipTests=true package

cp source-code/target/*.jar build-output/
cat source-code/pom.xml
cp source-code/pom.xml build-output/

## Send to repository manager
#mvn org.apache.maven.plugins:maven-deploy-plugin:2.8.2:deploy-file \
#  -Durl=$REPOSITORY_MANAGER_URL \
#  -DrepositoryId=repo \
#  -Dfile="build-output/${artifact_name}.jar" \
#  -Djavadoc="build-output/${artifact_name}-javadoc.jar" \
#  -Dsources="build-output/${artifact_name}-sources.jar" \
#  -DpomFile="source-code/pom.xml"

  

