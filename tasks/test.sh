#!/bin/bash
set -e -u -x

SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
. ${SCRIPTS_DIR}/settings.sh

env

# Run tests
mvn \
  org.jacoco:jacoco-maven-plugin:prepare-agent test -Dmaven.test.failure.ignore=false \
  verify sonar:sonar \
  -Dsonar.host.url=$SONAR_URL \
  -Dsonar.login=$SONAR_LOGIN

cp source-code/target/sonar/report-task.txt test-output
