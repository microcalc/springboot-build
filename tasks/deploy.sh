#!/bin/bash
set -e -u -x

tmp_path="$(mktemp -d)"

env
export VERSION=$(cat docker-image/docker_inspect.json | jq -r '.[0].Config.Labels["version"]')
export ARTIFACT=$(cat docker-image/docker_inspect.json | jq -r '.[0].Config.Labels["artifact"]')
export GROUP=$(cat docker-image/docker_inspect.json | jq -r '.[0].Config.Labels["group"]')

echo "$GCLOUD_CONFIG" | base64 -d > gcloud.json

gcloud auth activate-service-account --key-file gcloud.json

gcloud container clusters get-credentials "$GCLOUD_CLUSTER_NAME" --zone "$GCLOUD_CLUSTER_ZONE" --project "$GCLOUD_PROJECT"

kubectl version
kubectl cluster-info

ret=0
kubectl get svc $ARTIFACT || ret=$?

if [[ $ret == 0 ]]; then
  echo "Service exist"
else
  echo "Service not found"
  envsubst < "source-build/k8s-service.template.yml" > "$tmp_path/service.yml"
  kubectl create -f "$tmp_path/service.yml"
fi


envsubst < "source-build/k8s-deployment.template.yml" > "$tmp_path/deployment.yml"
kubectl apply -f "$tmp_path/deployment.yml"

