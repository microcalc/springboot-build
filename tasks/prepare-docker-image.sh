#!/bin/bash
set -e -u -x

env

cp artifact-repository/*.jar workspace

ls -lash artifact-repository
ls -lash workspace

#artifact_name=$(find artifact-repository -name "*.jar" -printf '%f')
artifact_name=$( \
  find artifact-repository \
    -maxdepth 1 \
    -regex "artifact-repository/${ARTIFACT}-[0-9]+\.[0-9]+\.[0-9]+\.jar" \
    -printf '%f' \
  )
version=$(echo "$artifact_name" | cut -d"-" -f4 | cut -d"." -f-3)
#artifact=$(echo "$artifact_name" | rev | cut -d"-" -f 2- | rev)


#cp source-build/image-create/docker/Dockerfile workspace
cat << EOF > "workspace/Dockerfile"
FROM openjdk:8-jdk-alpine
LABEL vendor=axpira \
      version=${version} \
      group=${GROUP} \
      artifact=${ARTIFACT}
VOLUME /tmp
COPY $artifact_name app.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java \$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar
EOF

echo $version > workspace/version


