#!/bin/bash
set -e -u -x


export $(cat test-output/report-task.txt | xargs)

id=$(curl -s $ceTaskUrl | jq -r '.task.analysisId')

status=$(curl $serverUrl/api/qualitygates/project_status?analysisId=$id | jq -r '.projectStatus.status')


if [[ "$status" = "ERROR" ]]; then
  exit 1
fi

