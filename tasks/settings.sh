#!/bin/bash

set -e -u

if $DEBUG; then
  set -x
fi

export ROOT_PATH="$(pwd)"
export M2_HOME=${ROOT_PATH}/.m2
export MAVEN_HOME=${ROOT_PATH}/.m2
export MAVEN_USER_HOME=${ROOT_PATH}/.m2
export M2_LOCAL_REPO=${ROOT_PATH}/.m2
mkdir -p "${M2_LOCAL_REPO}/repository"
cp $ROOT_PATH/source-build/settings.xml $M2_HOME

export SONAR_USER_HOME=${ROOT_PATH}/.sonar
export MAVEN_CONFIG="$M2_HOME/settings.xml"

export POM_VERSION=$(cat resource-version/version)
export POM_PATH="source-code/pom.xml"


function mvn(){
  /usr/bin/mvn -B -s "$MAVEN_CONFIG" -f $POM_PATH "$@"
}

mvn \
  org.codehaus.mojo:versions-maven-plugin:2.5:set \
  -DnewVersion="${POM_VERSION}" \
  -DprocessDependencies=false \
  -DprocessParent=false \
  -DprocessPlugins=false


