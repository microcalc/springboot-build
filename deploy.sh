#!/bin/bash
set -e

usage="$(basename "$0") <pom_path>

where:
  <pom_path> Pom.xml path of project to deploy

INFO
  You need to create a branch version
    git checkout --orphan version
    git rm --cached -rf .
    rm -rf *
    touch README.txt
    git add README.txt
    git commit -m \"Created branch\"
    git push origin version
"

help () {
  echo >&2 "$@"
  echo "$usage"
}

if [ "$#" -eq 0 ]; then
  help
  exit 1
fi

pom_path="$1"

if [ ! -f "$pom_path" ]; then
  echo "File '$pom_path' not found"
  exit 2
fi


scm_uri=$(mvn -B -f $pom_path org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.scm.connection | grep -v '^\[' | cut -d":" -f3-)
if [ -z "$scm_uri" ]; then
  echo "Check your pom scm.connection not found"
  exit 3
fi

group=$(mvn -B -f $pom_path org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.groupId | grep -v '^\[')
if [ -z "$group" ]; then
  echo "Check your pom groupId not found"
  exit 3
fi

artifact=$(mvn -B -f $pom_path org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.artifactId | grep -v '^\[')
if [ -z "$artifact" ]; then
  echo "Check your pom artifactId not found"
  exit 3
fi

echo "Creating deploy for [$group:$artifact] in [$scm_uri]"


fly --target ci \
  set-pipeline --pipeline "$artifact" \
  --config pipeline.yml \
  --var "source-repository-uri=$scm_uri" \
  --var "artifact=$artifact" \
  --var "group=$group" \
  --var "private-repo-key=$(cat ~/.ssh/id_rsa)" \
  -l "settings.yml"

fly -t ci unpause-pipeline -p $artifact

